//
//  KargoTests.swift
//  KargoTests
//
//  Created by set it solution on 06/04/2016.
//  Copyright © 2016 set it solution. All rights reserved.
//

import XCTest
@testable import Kargo

class KargoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCreatingShipment() {
        
        let shipment = Shipment(goods: "banana", origin: "kul", destination: "JB", tonage: 2.3, price: 200)
        shipment.setRating(1000000)
        
        XCTAssert(shipment.rating <= 10, "Rating cannot be more than 10")
    }
    
}
