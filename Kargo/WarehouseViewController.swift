
//
//  warehouseViewController.swift
//  table view
//
//  Created by Hun Chew Ping on 4/8/16.
//  Copyright © 2016 set it solution. All rights reserved.
//

import UIKit

class WarehouseViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    
    var warehouseItems: [Warehouse] = []
    
    let wItemNameArray = ["Air Conditional", "Washing Machine", "Refrigerator", "Water Heater"]
    let wItemDestStateArray = ["Kuala Lumpur", "Shah Alam", "Subang Jaya", "Petaling Jaya"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        func pumpWarehouseData()
        {
            
            for (var i :Int = 0 ; i < wItemNameArray.count ; i++ )
            {
                let wData : Warehouse = Warehouse(itemName:wItemNameArray[i], itemDestination:wItemDestStateArray[i])
                wData.itemImage = UIImage(named: wData.itemName)
                
                self.warehouseItems.append(wData)

            }
        }
        
        pumpWarehouseData()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

//extension of this specific viewcontrollerclass (warehouseViewController)
extension WarehouseViewController : UITableViewDataSource, UITableViewDelegate {
    
    //TableViewDataSource control
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return warehouseItems.count
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell!

        let wItem : Warehouse = self.warehouseItems[indexPath.row]
        
        
        //cell.textLabel?.text = wItem.itemName
        //cell.detailTextLabel?.text = wItem.itemDestination
        //cell.imageView?.image = wItem.itemImage
        

        let wItemName :UILabel? = cell.viewWithTag(1) as? UILabel
        wItemName?.text = wItem.itemName
        
        let wItemDestination :UILabel? = cell.viewWithTag(2) as? UILabel
        wItemDestination?.text = wItem.itemDestination

        let wItemImage :UIImageView? = cell.viewWithTag(3) as? UIImageView
        //wItemImage?.image = UIImage(named: wItem.itemName)
        wItemImage?.image = wItem.itemImage
    
        

        

        return cell
    }
    
    
    //TableViewDelegate control
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //navigate to another VC
        let wDetailsVC: WarehouseDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WarehouseDetailViewController") as! WarehouseDetailViewController
        
        let wItem : Warehouse = self.warehouseItems[indexPath.row]
        wDetailsVC.warehouseItem = wItem

        self.navigationController?.pushViewController(wDetailsVC, animated: true)

    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 100.0
    
    
    
}

















