//
//  shipment.swift
//  table view
//
//  Created by set it solution on 02/04/2016.
//  Copyright © 2016 set it solution. All rights reserved.
//

import Foundation

class Shipment {
    var goods: String
    var origin: String
    var destination: String
    var tonage: Double
    var price: Int
    
    var rating: Int
    
    init(goods: String, origin: String, destination: String, tonage: Double, price: Int) {
        self.goods = goods
        self.origin = origin
        self.destination = destination
        self.tonage = tonage
        self.price = price
        
        self.rating = 0
    }
    
    func setRating(rating: Int) {
        if (rating <= 10) {
            self.rating = rating
//        } else {
//            self.rating = 10
//        }
        }
    }
}