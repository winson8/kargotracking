//
//  ShipmentDetail.swift
//  Kargo
//
//  Created by set it solution on 09/04/2016.
//  Copyright © 2016 set it solution. All rights reserved.
//

import Foundation
import UIKit

class ShipmentDetailViewController : UIViewController {
    
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var goodLabel: UILabel!
    
    @IBOutlet weak var destinationLabel: UILabel!
    
    @IBOutlet weak var tonageLabel: UILabel!
  
    @IBOutlet weak var priceLabel: UILabel!
    var shipment: Shipment!
    
    override func viewWillAppear(animated: Bool) {
        self.goodLabel.text = "Goods: " + self.shipment.goods
        
        self.originLabel.text = "Origin: " + self.shipment.origin
        self.destinationLabel.text = "Destination: " + self.shipment.destination
        self.tonageLabel.text = "Tonage: \(self.shipment.tonage) Tons"
        self.priceLabel.text = "Shipping Cost RM \(self.shipment.price)"
    }
    
}