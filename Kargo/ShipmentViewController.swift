//
//  ViewController.swift
//  table view
//
//  Created by set it solution on 02/04/2016.
//  Copyright © 2016 set it solution. All rights reserved.
//

import UIKit

class ShipmentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var shipments: [Shipment] = [
        Shipment(goods: "fruits", origin: "Klang", destination: "JB", tonage: 15, price: 100),
        Shipment(goods: "vegetables", origin: "Klang", destination: "JB", tonage: 15, price: 100),
        Shipment(goods: "meat", origin: "Klang", destination: "JB", tonage: 15, price: 100),
        Shipment(goods: "mineral water", origin: "Klang", destination: "JB", tonage: 15, price: 100),
        Shipment(goods: "fish", origin: "Klang", destination: "JB", tonage: 15, price: 100)
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowShipmentDetails" {
            let shipmentDetailViewController: ShipmentDetailViewController = segue.destinationViewController as! ShipmentDetailViewController
            shipmentDetailViewController.shipment = self.shipments[self.tableView.indexPathForSelectedRow!.row]
        }
    }
}

extension ShipmentViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shipments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        
//        cell.shipment = self.shipments[indexPath.row]
        
        cell.textLabel?.text = self.shipments[indexPath.row].goods
        cell.detailTextLabel?.text = self.shipments[indexPath.row].origin + "-" + self.shipments[indexPath.row].destination
        return cell
    }
}






















