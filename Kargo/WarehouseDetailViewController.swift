//
//  WarehouseDetailViewController.swift
//  Kargo
//
//  Created by Hun Chew Ping on 4/15/16.
//  Copyright © 2016 set it solution. All rights reserved.
//

import UIKit

class WarehouseDetailViewController: UIViewController {
    
    var warehouseItem : Warehouse!

    @IBOutlet var itemName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.itemName.text = self.warehouseItem.itemName
        
        //self.navigationItem.title = "Warehouse"

        //HCP test
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

}
