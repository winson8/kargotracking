//
//  warehouse.swift
//  table view
//
//  Created by Hun Chew Ping on 4/9/16.
//  Copyright © 2016 set it solution. All rights reserved.
//

import Foundation
import UIKit

class Warehouse {
    
    var itemName :String
    var itemBrand :String?
    var itemDestination :String?
    var itemPostCd :String?
    var itemDesc :String?
    var weight :Float = 0.0
    var itemImage :UIImage?
    
    init(itemName:String, itemDestination:String)
    {
        self.itemName = itemName
        self.itemDestination = itemDestination
    }
    
    
}